import java.util.List;

public class MyArrayList {
    
    private int size; 
    private int[] result; 

    public MyArrayList(int size) {
        this.size = size; 
        result = new int[size]; 
    }

/**
 * This function returns a string representation of the result array
 * 
 * @return The string representation of the result array.
 */
    public String toString() {
        String answer = "";
        for(int i = 0; i < size; i++) {
            answer+= result[i];
        } 
        return answer; 
    }

/**
 * Set the value at the given index to the given value
 * 
 * @param index The index of the array element to set.
 * @param value The value to be set.
 */
    public void set(int index, int value) {
        result[index] = value; 
    }

/**
 * Given an index, return the value at that index
 * 
 * @param index The index of the element to get.
 * @return The value of the array at the given index.
 */
    public int get(int index) {
        return result[index]; 
    }

/**
 * Add a value to the end of the array.
 * 
 * @param value The value to be added to the end of the array.
 */
    public void add(int value) {
        int[] second = new int[size+1]; 
        for(int i = 0; i < size; i++) {
            second[i] = result[i]; 
        }
        second[size] = value; 
        this.result = second; 
        this.size++; 
    }

/**
 * Remove the element at the specified index from the result array.
 * 
 * @param index The index of the element to remove.
 */
    public void remove(int index) {
        for(int i = index; i < size - 1; i++) {
            result[i] = result[i+1];
        }
        size--; 
    }

/**
 * Given an array of integers, return true if the array contains a value
 * 
 * @param value the value to check for in the array
 * @return The method contains returns true if the value is in the array, false otherwise.
 */
    public boolean contains(int value) {
        for(int i = 0 ; i < result.length; i++) {
            if(result[i] == value) {
                return true; 
            }
        }
        return false; 
    }
}


